Demo Repository
---------------

This is a demo repository to demostrate a bug on GitLab where the
pipeline notifications that is shown on merge request lists the same
artifacts for multiple pipeline notifications.
